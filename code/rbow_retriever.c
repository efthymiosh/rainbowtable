#include "datastructures.h"

pthread_mutex_t printmut = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
    int id;
    Bloom_filter *bf;
    Plaintext_pair *table;
    uint64_t table_portion;
} Threadata;

void *thread_builder(void *);

int main(void) {
    uint64_t table_size, check;
    int i, err, found = 0;
    pthread_t *threads;
    Bloom_filter *bf;
    Plaintext_pair *rainbowtable, *result;
    char hash_buffer[257];
    int amt_tables, amt_threads = sysconf(_SC_NPROCESSORS_ONLN);

    {
        FILE *bfd;
        int cl;
        bfd = fopen("prop.dat", "r");
        if (!fread(&amt_tables, sizeof(int), 1, bfd) || !fread(&cl, sizeof(int), 1, bfd)) {
            fprintf(stderr, "Error reading prop.dat\n");
            return EXIT_FAILURE;
        }
        fclose(bfd);
        printf("Amount of tables: %d\nChain Length:%d\n", amt_tables, cl);
    }

    table_size = SEARCH_SPACE * SPACE_MULTIPLIER / CHAIN_LENGTH;
    threads = malloc(sizeof(pthread_t) * ((amt_threads > amt_tables) ? amt_threads : amt_tables));
    rainbowtable = malloc(table_size * (sizeof(Plaintext_pair)));
    bf = init_bloom(BLOOMFILTER_SIZE, BLOOMFILTER_MUTEXES); /* size 1GB, 32768 mutexes for fine-grained protection */
    if (!threads || !rainbowtable || !bf || !(bf->bloom) || !(bf->mutarr)) {
        fprintf(stderr, "Unable to allocate enough memory for the structures\n");
        return EXIT_FAILURE;
    }

    /* spawn structure builder threads */
    printf("Building Rainbow Table & Bloom Filter..\n");
    for (i = 1; i < amt_tables; i ++) {
        Threadata *td = malloc(sizeof(Threadata));
        td->id = (int)i;
        td->bf = bf;
        td->table = rainbowtable;
        td->table_portion = table_size / amt_tables;
        pthread_create(threads + i, NULL, thread_builder, (void *)td);
    }
    Threadata *td = malloc(sizeof(Threadata));
    unsigned long long cnt = 0;
    td->id = 0;
    td->bf = bf;
    td->table = rainbowtable;
    td->table_portion = table_size / amt_tables;
    cnt = (unsigned long long int)thread_builder((void *)td);
//     printf("Inserted: %llu words, omitted %llu\n", cnt, (unsigned long long int) table_size / amt_tables - cnt);
    for (i = 1; i < amt_threads; i ++) {
        unsigned long long cnt;
        if (err = pthread_join(threads[i], (void*) &cnt))
            fprintf(stderr, "%s\n", strerror(err));
//         printf("Inserted: %llu words, omitted %llu\n", cnt, (unsigned long long int) table_size / amt_tables - cnt);
    }
    printf("..done\nChecking Bloom Filter..\n");
    uint64_t sum = 0, bloom_bytesize = bf->bitsize / BYTESIZEINBITS;
    for (uint64_t iter = 0; iter < bloom_bytesize; iter++) {
        sum +=
            !!(bf->bloom[iter] & 0x01) +
            !!(bf->bloom[iter] & 0x02) +
            !!(bf->bloom[iter] & 0x04) +
            !!(bf->bloom[iter] & 0x08) +
            !!(bf->bloom[iter] & 0x10) +
            !!(bf->bloom[iter] & 0x20) +
            !!(bf->bloom[iter] & 0x40) +
            !!(bf->bloom[iter] & 0x80);
    }
    printf("..done, %.4f%% congestion\nSorting Rainbow Table..\n", ((double)sum)/bf->bitsize * 100);
    qsort((void *)(rainbowtable), table_size, sizeof(Plaintext_pair), pair_compar);
    printf("..done\nYou may enter hashes (type \"h\" or \"help\" for help)\n");
    for (;;) {
        memset(hash_buffer, '\0', 257);
        printf("> ");
        if (!fgets(hash_buffer, 257, stdin)) {
            fprintf(stderr, "Unable to read from input\n");
            break;
        }
        if (hash_buffer[256] != '\0') {
            do {
                hash_buffer[256] = '\0';
                if (!fgets(hash_buffer, 257, stdin)) {
                    fprintf(stderr, "Unable to read from input\n");
                    return EXIT_SUCCESS;
                }
            } while (hash_buffer[256] != '\0');
            printf("Invalid input\n");
            continue;
        }
        if (!strcmp(hash_buffer, "h\n") || !strcmp(hash_buffer, "help\n")) {
            printf("COMMAND     SHORTCUT FUNCTION\n"
                   "help        h        Prints this message\n"
                   "print       p        Prints the entire rainbow table\n"
                   "quit        q        Exits the program\n"
                   "check D     c D      If D is a valid BLAKE256 hash an attempt to retrieve the password it encrypts it is made\n");
        }
        else if(!strcmp(hash_buffer, "p\n") || !strcmp(hash_buffer, "print\n")) {
            char printtext[PASS_LENGTH];
            for (int i = 0; i < table_size; i++) {
                num_to_plaintext(printtext, rainbowtable[i].initial);
                printf("%s--> .. -->", printtext);
                for (int o = 0; o < 32; o++)
                    printf("%02hhx", rainbowtable[i].final[o]);
                putchar('\n');
            }
            continue;
        }
        else if(!strcmp(hash_buffer, "q\n") || !strcmp(hash_buffer, "quit\n")) {
            printf("Where are you going, master?\n");
            break;
        }
        else if(!strncmp(hash_buffer, "c ", check = 2) || !strncmp(hash_buffer, "check ", check = 6)) {
            char *str_start = hash_buffer + check;
            char *str_end, blake_out[32], inithash[32] = {0}, plaintext[PASS_LENGTH + 1] = {0};
            int k;
            unsigned long long int xxhash_out;
            strtoull(str_start, &str_end, 0x10); /* just for the error checking */
            if (str_start == str_end || *str_end != '\n' || str_start + 64 != str_end) {
                printf("String provided is not a BLAKE256 hash in text form\n");
                continue;
            }
            *str_end = '\0';
            char ch;
            for (i = 0; i < 64; i++) { /* convert the text hash to the form the SHA3 function returns to */
                if ((ch = str_start[i] - '0') > 9) ch -= ('a' - '0' - 10);
                inithash[i >> 1] |= ch << (!(i & 1) << 2);
            }
            /* Retrieve the password */
            int tst = 0;
            for (i = CHAIN_LENGTH - 1; i != UINT64_MAX; --i) {
                memcpy(blake_out, inithash, 32); /* the password hash */
                for (k = i; k < CHAIN_LENGTH - 1; k++) {
                    reduce(k, plaintext, &xxhash_out, blake_out);
                    blake256_hash(blake_out, plaintext, PASS_LENGTH);         /* Get the BLAKE256 hash for the next iteration */
                }
                if (!bloom_contains(bf, blake_out, 3))
                    continue;
                /* The final hash is (most probably) in the table, find it (binary search) */
                result = bin_search(rainbowtable, table_size, blake_out);
                if (!result)
                    continue; /* false positive from the bloom filter */
                while (result != rainbowtable && !memcmp((result - 1)->final, result->final, 32))
                    --result;
                int8_t *final = result->final; /* immutable */
                while (!memcmp(final, result->final, 32)) {
                    uint64_t initial = result->initial;                 /* for each match, get the initial value */
                    num_to_plaintext(plaintext, initial);               /* Get the initial plaintext in string form */
                    for (k = 0; k < i; k++) {                           /* Rebuild the chain until you reach the hash */
                        blake256_hash(blake_out, plaintext, PASS_LENGTH);
                        reduce(k, plaintext, &xxhash_out, blake_out);
                    }
                    blake256_hash(blake_out, plaintext, PASS_LENGTH);
                    if (!memcmp(blake_out, inithash, 32)) { /* hashes same, found! */
                        printf("%s\n", plaintext);
                        found = 1; /* ludicrous */
                        break;
                    }
                    /* Plaintext could now be the password, if a merge wasn't done */
                    ++result;
                }
                if (found) /* appauling */
                    break;
            }
            if (found) /* disgusting */
                found = 0;
            else
                printf("Not found..\n");
        }
        else {
            printf("Invalid input\n");
            continue;
        }
    }
    return EXIT_SUCCESS;
}

void *thread_builder(void *data) {
    Threadata *td = (Threadata *) data;
    unsigned long long int cnt = 0;
    int j, index = td->id * td->table_portion;
    int portion = td->table_portion;
    Bloom_filter *bf = td->bf;
    char file_in[7], plaintext[PASS_LENGTH + 1];
    FILE *fd;
    snprintf(file_in, 7, "%d.log", td->id);
    fd = fopen(file_in, "r");
    if (fd == NULL) {
        fprintf(stderr, "Unable to open file %s\n", file_in);
        return NULL;
    }
    for (j = 0; j < portion; j++) {
        int8_t *final = td->table[index + j].final;
        if (!fread(&(td->table[index + j].initial), sizeof(uint64_t), 1, fd) ||
            !fread(final, sizeof(uint8_t), 32, fd)
        ) {
            return NULL;
        }
        if(!bloom_contains(bf, final, 3)) {
            bloom_insert_word(bf, final, 3);
            ++cnt;
        }
    }
    fclose(fd);
    free(data);
    return (void*)cnt;
}
