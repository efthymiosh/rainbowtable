#include "datastructures.h"

/**
 * ASCII representations of valid characters
 * '!'       : 33       | 0
 * ['0'-'9'] : [48-57]  | 1 - 10
 * '@'       : 64       | 11
 * ['A'-'Z'] : [65-90]  | 12 - 37
 * ['a'-'z'] : [97-122] | 38 - 63
 */

/* magic number extravaganza */
uint64_t plaintext_to_num(char *plaintext) {
    int i;
    uint64_t result = 0;
    for (i = 0; i < PASS_LENGTH; i ++) {
        uint64_t digit, ch = plaintext[PASS_LENGTH - 1 - i];
        if (ch <= 'Z') {
            if (ch <= '9') {
                if (ch == '!')
                    digit = 0; //0
                else
                    digit = ch - ('0' - 1); //1 - 10
            }
            else
                digit = ch - ('@' - 11); //11 - 37
        }
        else
            digit = ch - ('a' - 38); //38 - 63
        result |= digit << (i*6);
    }
    return result;
}

/* magic number extravaganza, assumes PASS_LENGTH + 1 char preallocated buffer @ plaintext */
void num_to_plaintext(char *plaintext, uint64_t num) {
    int i;
    plaintext[PASS_LENGTH] = '\0';
    num &= PASS_MASK;
    for (i = PASS_LENGTH - 1; i >= 0; --i) {
        uint64_t digit = num & 0x3f;
        num >>= 6;
        if (digit < 11) {
            if (!digit)
                plaintext[i] = '!';
            else
                plaintext[i] = ('0' - 1) + digit;
        }
        else if (digit < 38)
            plaintext[i] = ('@' - 11) + digit;
        else
            plaintext[i] = ('a' - 38) + digit;
    }
    return;
}

void reduce(int k, char *plaintext, unsigned long long *num, uint8_t *hash) {
    *num = XXH64(hash, 23, k);               /* Hash it with xxHash(k) (Reduce pt.1) */
//     uint8_t c;
//     *num = k;
//     int sz = 32;
//     while (sz--) {
//         c = *(hash++);
//         *num = ((*num << 5) + *num) + c; /* hash * 33 + c */
//     }
    num_to_plaintext(plaintext, *num);       /* Get a plaintext from it (Reduce pt.2) */
}
