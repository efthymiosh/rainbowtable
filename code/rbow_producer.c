#include "datastructures.h"

pthread_mutex_t printmut = PTHREAD_MUTEX_INITIALIZER;
int printctr;
int amt_threads;

typedef struct {
    int id;
    uint64_t portion;
} Threadata;

void *thread_fn(void *);

int main(void) {
    uint64_t i, portion;
    int err;
    pthread_t *threads;
    amt_threads = sysconf(_SC_NPROCESSORS_ONLN);
    portion = SEARCH_SPACE  * SPACE_MULTIPLIER / amt_threads;
    printf("Amount of hardware threads: %d\n", amt_threads);
    if (amt_threads == -1)
        amt_threads = 2;
    threads = malloc(sizeof(pthread_t) * amt_threads);

    { /* for emphasis */
        FILE *fd;
        FILE *bfd;
        int cl = CHAIN_LENGTH;
        fd = fopen("prop.log", "w"); /* human-readable */
        bfd = fopen("prop.dat", "w");

        fprintf(fd, "Tables: %d\n"
                    "Space portion: %"PRIu64"\n"
                    "Chain Length: %d\n",
                amt_threads, portion, cl);
        fwrite(&amt_threads, sizeof(int), 1, bfd);
        fwrite(&cl, sizeof(int), 1, bfd);
        fclose(fd);
        fclose(bfd);
    }


    /* spawn threads */
    for (i = 1; i < amt_threads; i ++) {
        Threadata *td = malloc(sizeof(Threadata));
        td->id = (int)i;
        td->portion = portion;
        pthread_create(threads + i, NULL, thread_fn, (void *)td);
    }
    Threadata *td = malloc(sizeof(Threadata));
    td->id = 0;
    td->portion = portion;
    thread_fn(td);
    for (i = 1; i < amt_threads; i ++)
        if (err = pthread_join(threads[i], NULL))
            fprintf(stderr, "%s\n", strerror(err));
    return EXIT_SUCCESS;
}

void *thread_fn(void *data) {
    Threadata *td = (Threadata *) data;
    uint64_t reps, portion = td->portion, percent;
    char plaintext[PASS_LENGTH + 1], file_out[7];
    char blake_out[32];
    unsigned long long xxhash_out, tot = portion / CHAIN_LENGTH;
    struct drand48_data rand_state;
    srand48_r(((long int) pthread_self()), &rand_state);
    int i, j, k, pcnt_ctr = 0;
    long int rn;
    FILE *fd;
    struct timespec time_start;
    struct timespec time_elapsed;
    clockid_t cid;
    percent = portion / CHAIN_LENGTH / 100;
    Plaintext_pair *table = malloc (portion / CHAIN_LENGTH * sizeof(Plaintext_pair));
    pthread_getcpuclockid(pthread_self(), &cid);
    if (clock_gettime(cid, &time_start) == -1)
        return NULL;
    if (!table) {
        pthread_mutex_lock(&printmut);
        printf("Unable to allocate memory for table, memory size: %lu bytes\n", portion / CHAIN_LENGTH * sizeof(Plaintext_pair));
        pthread_mutex_unlock(&printmut);
        return NULL;
    }
    /* (Un)Fortunately, blake256_hash is so annoyingly slow that the extra counters' overhead is irrelevant */
    for (j = 0, i = 0, reps = 0; reps < portion; j++, i++, reps += CHAIN_LENGTH) {
        xxhash_out = 0;
        /*option 1*/
//         for (int jj = 0; jj < PASS_LENGTH; jj++) { /* too slow? */
//             lrand48_r(&rand_state, &rn);
//             xxhash_out <<= 6;
//             xxhash_out |= rn & 0x3f;
//         }
        /*option 2*/
//         lrand48_r(&rand_state, &rn);
//         xxhash_out |= rn & 0x3f;
//         xxhash_out <<= 7;
//         lrand48_r(&rand_state, &rn);
//         xxhash_out ^= rn & PASS_MASK;
        /*option 3*/
        lrand48_r(&rand_state, &rn);
        xxhash_out = XXH64(&rn, sizeof(long int), (long long)time(NULL));
        xxhash_out >>= 64 - PASS_LENGTH * 6;

        table[i].initial = xxhash_out;             /* initial plaintext */
        num_to_plaintext(plaintext, xxhash_out);                /* Get the initial plaintext in string form */
        for (k = 0; k < CHAIN_LENGTH; k++) {
            blake256_hash(blake_out, plaintext, PASS_LENGTH);   /* Get the BLAKE256 hash for the plaintext */
            reduce(k, plaintext, &xxhash_out, blake_out);       /* Reduce back to a plaintext */
        }
        memcpy(table[i].final, blake_out, 32);
        if (j == percent) {
            j = 0;
            ++pcnt_ctr;
            if (clock_gettime(cid, &time_elapsed) == -1) {
                pthread_mutex_lock(&printmut);
                fprintf(stderr, "Failed to get clock.\n");
                pthread_mutex_unlock(&printmut);
                return NULL;
            }

            pthread_mutex_lock(&printmut);
            if (++printctr == amt_threads) {
                printctr = 0;
                printf("%03d%% in %lds\n", pcnt_ctr, time_elapsed.tv_sec - time_start.tv_sec);
            }
            pthread_mutex_unlock(&printmut);
        }
    }
    snprintf(file_out, 7, "%d.log", td->id);
    fd = fopen(file_out, "w");
    for (j = 0; j < i; j++) {
        fwrite(&(table[j].initial), sizeof(uint64_t), 1, fd);
        fwrite(table[j].final, sizeof(uint8_t), 32, fd);
    }
    fclose(fd);
    free(data);
    return table;
}
