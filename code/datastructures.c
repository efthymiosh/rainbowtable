#include "datastructures.h"

Bloom_filter *init_bloom(uint64_t size, int mutex_amt) {
    int i, bytesize;
    Bloom_filter *retval = malloc (sizeof(Bloom_filter));
    retval->bloom = calloc ((size / 8 + !!(size % 8)), sizeof(char));
    retval->bitsize = size;
    bytesize = size / BYTESIZEINBITS + !!(size % BYTESIZEINBITS);
    retval->mutarr = malloc(mutex_amt * sizeof(pthread_mutex_t));
    for (i = 0; i < mutex_amt; i++)
        pthread_mutex_init(retval->mutarr + i, NULL);
    retval->bytes_per_mutex = bytesize / mutex_amt + !!(bytesize % mutex_amt);
    return retval;
}

int ull_compar(const void *fst, const void *snd) {
    return (int)((*(unsigned long long*)fst) - (*(unsigned long long*)snd));
}

int bloom_insert_word(Bloom_filter *bf, int8_t *hash, int amt_hash) {
    uint64_t whichbyte, oldbyte;
    unsigned long long *results;
    uint64_t mutoffset, prevmutoffset;
    int iter = amt_hash;
    results = malloc (sizeof(uint64_t) * amt_hash);
    do {
        results[--iter] = XXH64(hash, 32, iter) % bf->bitsize;
    } while (iter);
    qsort((void *)results, amt_hash, sizeof(unsigned long long), ull_compar);

    mutoffset = prevmutoffset = results[0] / (BYTESIZEINBITS * sizeof(char)) / bf->bytes_per_mutex;
    pthread_mutex_lock(bf->mutarr + mutoffset);
    for (iter = 1; iter < amt_hash; ++iter) {
        if (prevmutoffset == (mutoffset = results[iter] / (BYTESIZEINBITS * sizeof(char)) / bf->bytes_per_mutex))
            continue;
        pthread_mutex_lock(bf->mutarr + mutoffset);
        prevmutoffset = mutoffset;
    }

    whichbyte = results[0] / (BYTESIZEINBITS * sizeof(char));
    prevmutoffset = whichbyte / bf->bytes_per_mutex;
    for (iter = 0; iter < amt_hash; iter++) {
        bf->bloom[whichbyte] |= 1 << (results[iter] % (BYTESIZEINBITS * sizeof(char)));
        if (iter + 1 < amt_hash) {
            whichbyte = results[iter + 1] / (BYTESIZEINBITS * sizeof(char));
            mutoffset =  whichbyte / bf->bytes_per_mutex;
            if (prevmutoffset == mutoffset)
                continue;
        }
        pthread_mutex_unlock(bf->mutarr + prevmutoffset);
        prevmutoffset = mutoffset;
    }
    free(results);
    return 0;
}

int bloom_contains(Bloom_filter *bf, int8_t *hash, int amt_hash) {
    unsigned long long *results;
    int iter = amt_hash, whichbyte, tst;
    results = malloc (sizeof(uint64_t) * amt_hash);
    do {
        results[--iter] = XXH64(hash, 32, iter) % bf->bitsize;
    } while (iter);
    qsort((void *)results, amt_hash, sizeof(unsigned long long), ull_compar);

    for (iter = 0; iter < amt_hash; iter++) {
        whichbyte = results[iter] / (BYTESIZEINBITS * sizeof(char));
        pthread_mutex_lock(bf->mutarr + whichbyte / bf->bytes_per_mutex);

        tst = !(bf->bloom[whichbyte] & (1 << (results[iter] % (BYTESIZEINBITS * sizeof(char)))));

        pthread_mutex_unlock(bf->mutarr + whichbyte / bf->bytes_per_mutex);
        if (tst) {
            free(results);
            return 0;
        }
    }
    free(results);
    return 1;
}

void bloom_purge(Bloom_filter **bf) {
    free((*bf)->bloom);
    free(*bf);
    *bf = NULL;
    return;
}

Plaintext_pair *bin_search(Plaintext_pair *rainbow, int rbow_size, uint8_t final[32]) {
    Plaintext_pair *mid_slice = rainbow + (rbow_size >> 1);
    if (rbow_size == 0)
        return NULL;
    int res = memcmp(mid_slice->final, final, 32);
    if (res == 0)
        return mid_slice;
    if (res > 0)
        return bin_search(rainbow, rbow_size >> 1, final);
    return bin_search(mid_slice, rbow_size >> 1, final);
}

int pair_compar(const void *fst, const void *snd) {
    uint8_t *final = ((Plaintext_pair*)fst)->final;
    uint8_t *finalsnd = ((Plaintext_pair*)snd)->final;
    return memcmp(final, finalsnd, 32);
}
