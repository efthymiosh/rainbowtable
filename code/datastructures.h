#ifndef MYDATASTRUCTURES
#define MYDATASTRUCTURES

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../BLAKE/blake.h"
#include "../xxHash/xxhash.h"

#define BYTESIZEINBITS 8

/* how many search spaces to cover if all cells of rainbow table make up for the entire search space?  */
#define SPACE_MULTIPLIER 2

/* password data. must be calculated by programmer before changing */
 #define CHAIN_LENGTH 2048
 #define PASS_LENGTH 6
 #define PASS_MASK 0xfffffffff
 #define SEARCH_SPACE 68719476736ULL
 #define BLOOMFILTER_SIZE 1589934592ULL
 #define BLOOMFILTER_MUTEXES 32768


/*pass len 4*/
// #define CHAIN_LENGTH 512
// #define PASS_LENGTH 4
// #define PASS_MASK 0xffffff
// #define SEARCH_SPACE 16777216ULL
// #define BLOOMFILTER_SIZE 1589934592ULL
// #define BLOOMFILTER_MUTEXES 32768

/*pass len 3*/
// #define CHAIN_LENGTH 512
// #define PASS_LENGTH 3
// #define PASS_MASK 0x3ffff
// #define SEARCH_SPACE 262144ULL
// #define BLOOMFILTER_SIZE 1589934592ULL
// #define BLOOMFILTER_MUTEXES 32768

/*pass len 2*/
// #define CHAIN_LENGTH 2
// #define PASS_LENGTH 2
// #define PASS_MASK 0xfff
// #define SEARCH_SPACE 4096ULL
// #define BLOOMFILTER_SIZE 1589934592ULL
// #define BLOOMFILTER_MUTEXES 32768

uint64_t plaintext_to_num(char *plaintext);
void num_to_plaintext(char *plaintext, uint64_t);
void reduce(int k, char *plaintext, unsigned long long *num, uint8_t *hash);

typedef struct {
    char *bloom;
    uint64_t bitsize;
    pthread_mutex_t *mutarr;
    int bytes_per_mutex;
} Bloom_filter;

typedef struct pair {
    uint64_t initial;
    uint8_t final[32];
} Plaintext_pair;

Bloom_filter *init_bloom(uint64_t size, int mutex_amt);
int bloom_insert_word(Bloom_filter *bf, int8_t *blakehash, int amt_hash);
int bloom_contains(Bloom_filter *bf, int8_t *blakehash, int amt_hash);
void bloom_purge(Bloom_filter **bf);

void *thread_fn(void *);

int pair_compar(const void *fst, const void *snd);
/**
 * Binary search, assumes sorted rainbow table.
 */
Plaintext_pair *bin_search(Plaintext_pair* rainbow, int rbow_size, uint8_t final[32]);
#endif
